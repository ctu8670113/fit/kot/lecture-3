package cz.ctu.fit.bi.kot.heap

interface Elem {
    val content: List<String>
    val w: Int
        get() = if (content.isEmpty()) 0
        else content.first().length //  content.first()?.length?:0
    val h: Int
        get() = content.size

    fun create(content: List<String>): Elem
    infix fun above(that: Elem): Elem {
        require(this.w == that.w)
        return create(this.content + that.content)
    }

    infix fun beside(that: Elem): Elem {
        require(this.h == that.h)
        return create(content.zip(that.content).map { it.first + it.second })
    }
}

abstract class AElem : Elem {
    override fun toString() = content.joinToString(separator = "\n")
}


open class BasicElem(final override val content: List<String>) : AElem() {
    init {
        if (content.isNotEmpty()) {
            val w = content.first().length
            require(content.all { w == it.length }) { "fail" }
        }
    }

    override fun create(content: List<String>) = BasicElem(content)
}

open class CharElem : BasicElem{
    constructor(c: Char, w: Int, h: Int) : super(
        List(h) { c.toString().repeat(w) }
    )
    constructor(string: String, w:Int, h: Int ) : super(
        (if(string.length== w) {
            List(h){ string }
        } else{
            (string.map { (it.toString()) })
        })
    )
}
open class EmptyElem(w: Int, h: Int) : CharElem(' ', w, h)

fun Elem.widenL(newWidth: Int): Elem {
    return EmptyElem(newWidth, this.h) beside this
}

fun Elem.widenR(newWidth: Int): Elem {
    return this beside EmptyElem(newWidth, this.h)
}

fun Elem.hightenU(newHeight: Int): Elem {
    return EmptyElem(this.w, newHeight) above this
}

fun Elem.hightenD(newHeight: Int): Elem {
    return this above EmptyElem(this.w, newHeight)
}

fun generateSpiral(size: Int): Elem {
    return if (size == 1) {
        CharElem('*', size, size)
    } else {
        return when (size % 4) {
            0 -> generateSpiral(size - 1).widenL(1) above CharElem('*', size, 1)
            1 -> CharElem('*', 1, size) beside generateSpiral(size - 1).hightenU(1)
            2 -> CharElem('*', size, 1) above generateSpiral(size - 1).widenR(1)
            else -> generateSpiral(size - 1).hightenD(1) beside CharElem('*', 1, size)
        }
    }
}
fun generateSpiral(size: Int, string: String): Elem {
    return if (size == 1) {
        CharElem(string.first(), size, size)
    } else {
        val firstSup = string.substring(0,size)
        val endSup = string.substring(size,string.length)
        return when (size % 4) {
            0 -> generateSpiral(size - 1,endSup).widenL(1) above CharElem(firstSup, size, 1)
            1 -> CharElem(firstSup, 1, size) beside generateSpiral(size - 1,endSup).hightenU(1)
            2 -> CharElem(firstSup.reversed(), size, 1) above generateSpiral(size - 1,endSup).widenR(1)
            else -> generateSpiral(size - 1,endSup).hightenD(1) beside CharElem(firstSup.reversed(), 1, size)
        }
    }
}
fun number(int: Int): Pair<Int, Int> {
    var n = 0
    while (true) {
        val sum = n * (n + 1)
        if (sum >= int)
            return Pair(sum, n)
        n++
    }
}

fun main() {

    println(generateSpiral(12))
    var string =
        ("Po celou dobu železniční přepravy bataliónu, který měl sklízet válečnou slávu," +
                " až projde pěšky od Laborce východní Haličí na front," +
                " vedly se ve vagónu, kde byl jednoroční dobrovolník a Švejk," +
                " opět podivné řeči, víceméně velezrádného obsahu.")
            .replace(" ","").replace(",","")
    val pair = number(string.length)
    string += " ".repeat(pair.first-string.length)
    val size= pair.second + if(pair.second/2 %2== 1) (pair.second/2) -1 else pair.second/2
    println(generateSpiral(size,string.reversed()))
}